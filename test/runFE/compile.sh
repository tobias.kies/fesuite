# This is "quick and dirty" file to make some hands-on experiments with the C++ library.
# In future, here should be "proper" unit tests and a more explicit usage documentation.

# You can compile the code like this:
clang++ runFE.cpp --include-directory "../../cpp" -std=c++17
# g++ runFE.cpp --include-directory "../../cpp" -std=c++17
