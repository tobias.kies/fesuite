#include <iostream>

#include "local_fe/hermitek.hh"
#include "assembler/operatorassembler.hh"
#include "assembler/localassembler/laplaceassembler.hh"
#include "global_fe/globalhermitek.hh"
#include "grid/tensorgrid.hh"
#include "global_fe/fe_function.hh"
#include "simple_interface/hermitefunction.hh"
#include "simple_interface/circlemassmatrix.hh"
#include "simple_interface/boundarymassmatrix.hh"

template<class Matrix>
const void printMatrix(const Matrix& M)
{
	for (int i = 0; i < M.rows(); ++i)
	{
		for (int j = 0; j < M.cols(); ++j)
			std::cout << M[i][j] << " ";
		std::cout << std::endl;
	}
}

template<class Matrix>
const void printMatrixPattern(const Matrix& M)
{
	for (int i = 0; i < M.rows(); ++i)
	{
		for (int j = 0; j < M.cols(); ++j)
			std::cout << (M[i][j] != 0) << " ";
		std::cout << std::endl;
	}
}

int main()
{
	using namespace FeSuite;

	const int dim = 2;
	const int order = 1;
	using Grid = TensorGrid::TensorGrid<dim>;
	using AnsatzSpace = HermiteAnsatzSpace<Grid, dim, order>;
	using LocalAssembler = LocalLaplaceAssembler<AnsatzSpace>;
	using Assembler = OperatorAssembler<AnsatzSpace, LocalAssembler>;
	using Function = FeFunction<AnsatzSpace>;

	Grid::DiscretizationVectors discVecs;
	for (int i = 0; i < dim; ++i)
	{
		discVecs[i].resize(4);
		for (unsigned int j = 0; j < discVecs[i].size(); ++j)
			discVecs[i][j] = 0.5*j;
	}
	Grid grid(discVecs);

	AnsatzSpace ansatzSpace(grid);

	LocalAssembler localAssembler;
	Assembler assembler(ansatzSpace, localAssembler);
	std::cerr << ".";
	const auto&& M = assembler.assemble();
	std::cerr << "." << std::endl;

	printMatrix(M);
	//std::cout << std::endl << std::endl;
	//printMatrixPattern(M);

	Function f(ansatzSpace);
	auto& c = f.getCoefficients();
	int counter = 0;
	for (unsigned int i = 0; i < c.size(); ++i)
		c[i] = (counter++)*((i % (int)std::pow(order, dim)) == 0);
	typename Function::DomainType x;
	for (unsigned int i = 0; i < x.size(); ++i)
		x[i] = 0.25;
	const auto&& y = f.evaluate(x, { 1 });

	std::cerr << "value at " << x;
	std::cerr << " is " << y << std::endl;


	HermiteFunction<dim, order> func(discVecs);
	auto& c_ = func.get_coefficients();
	counter = 0;
	for (unsigned int i = 0; i < c_.size(); ++i)
		c_[i] = (counter++)*((i % (int)std::pow(order, dim)) == 0);
	std::cerr << "again: value at " << x << " is " << func.evaluate(x, { 1 }) << std::endl;


	typename Function::DomainType center(0);
	const auto circleMatrix = assemble_circle_mass_matrix(func, center, 1);
	printMatrix(circleMatrix);

	const auto boundaryMassMatrix = assemble_boundary_mass_matrix(func);
	printMatrix(boundaryMassMatrix);

    return 0;
}