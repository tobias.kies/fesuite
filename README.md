# FeSuite
## About
FeSuite is a minimalistic C++ library for creating and handling discretizations in the context of finite element spaces.
Usually, this is the case when working with partial differential equations (PDEs), but there are also creative other ways to work with this library.

How FeSuite considers data structures for finite element discretizations is very similar to the how the [DUNE library](https://dune-project.org/) does (which is not just by chance, but it is much rather a direct consequence of the fact that I spent half of my [PhD studies](https://refubium.fu-berlin.de/bitstream/handle/fub188/24940/Dissertation_Tobias_Kies.pdf?sequence=1&isAllowed=y) with doing implementations in DUNE, e.g. developing [this](https://gitlab.com/tobias.kies/dune-particle)).

DUNE is more powerful than FeSuite, but it also is significantly harder to set up and master.
In that sense, FeSuite can be used to have a more light-weight access to some discretization methods, which makes it potentially useful for educational purposes and quick prototyping in python.

FeSuite allows to create arbitrary tensor-grid based discretizations of arbitrary-dimensional hybercubes, on which it can create finite element ansatz spaces of arbitrarily high order.
Currently, it supports assembling laplace and mass operators over domains and boundaries for these ansatz spaces.

The biggest weakness of FeSuite is the counterpart of its simplicity: not everything is geared towards optimal efficiency (yet) and there certainly are plentiful use cases for which would be quite hard or impossible to cover with this library.

## Installation Notes
While FeSuite was originally developed for both Linux and Windows environments, the  library is currently only tested with Ubuntu Linux.

If you already have `python3` installed, you can just run
```sh
./install.sh
```
and it will install the `fesuite` python package into you active environment.

## Sample Usage & Documentation
You can check out the examples in `python/examples` in order to a feeling for how it works.
If you are curious to see it in use in a somewhat (but not entirely) more serious setting, you can also check out the [Showcase Projection: Potato Oven Optimization](https://gitlab.com/tobias.kies/a-showcase-potato-oven-optimization) where it is used as part of an optimal control solver and comes into play for setting up finite element discretizations of an heat flow equation.

Sadly, there is no comprehensive documentation of this library available yet.

## To-Dos
Here is a list of open to-dos which might help to get a better idea of the state of this project:

### General
* generally, more documentation would be great
* add proper unit tests, both for C++ and Python
* add CI pipeline for quality assurance

### C++
* make global ansatz spaces inherit from a common virtual interface.
* make local ansatz spaces inherit from a common virtual interface.
* make grids inherit from a common virtual interface.
* make operator assembler use sparse matrices.
* consider throwing better errors
* introduce proper SFINAE for operator T() method in fieldarray.hh

### Python
* test installation/compilation in a well-defined docker environment
