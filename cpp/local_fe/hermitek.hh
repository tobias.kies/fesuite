#ifndef _FESUITE_LOCAL_FE_HERMITEK_
#define _FESUITE_LOCAL_FE_HERMITEK_

#include "util/fieldarray.hh"
#include "util/factorialcache.hh"
#include "util/hermitecache.hh"
#include "util/power.hh"

#include <array>
#include <assert.h>
#include <cmath>

namespace FeSuite
{
	template<class R, class D, int k>
	struct HermiteKLocalBasisEvaluator
	{
		inline static R evaluate1D(const D& x, const int function_index, const int order_derivative)
		{
			// Note that the i-th basis function is given by
			//     sum_{j=i}^{k-1} coeff(i,j) * x^j * (x-1)^k.
			// Thus the l-th derivative is given by
			//    sum_{j=i}^{k-1} sum_{m=0}^l coeff(i,j) * (l choose m) * j! / (j-l+m)! * x^(j-l+m) * k! / (k-m)! * (x-1)^(k-m)
			// where a/b! := 0 for b < 0 and a arbitrary (even a = inf).
			/// Note that the following implementation certainly is not the most efficient or stable!

			// If the order of the derivative is higher than the polynomial order, then we can directly return 0.
			if (order_derivative > 2 * k - 1) return 0;

			R ret = 0;
			std::array<R, k + 1> xpow, x1pow;
			xpow[0] = 1; x1pow[0] = 1;
			for (int j = 1; j < (int) xpow.size(); ++j)
			{
				xpow[j] = xpow[j - 1] * x;
				x1pow[j] = x1pow[j - 1] * (x - 1);
			}

			const auto& i = function_index;
			const auto& l = order_derivative;
			const auto&& factorial = [](const auto x) {return FactorialCache::evaluate(x); };
			const auto& hermite = HermiteCache::evaluate(k);

			int mMax = std::min(order_derivative, k);
			for (int j = i; j < k; ++j)
				for (int m = std::max(l - j, 0); m <= mMax; ++m)
					// Note: For stability reasons the line below should rather not do "full numerator divided by full denominator".
					//		 This presentation is solely chosen for readibility reasons and should be improved once it is deemed
					//		 necessary enough.
					ret += hermite[i][j] * factorial(l) *factorial(j) * factorial(k)
							/ (factorial(m) * factorial(l - m) * factorial(j - l + m) * factorial(k - m))
							* xpow[j - l + m] * x1pow[k - m];

			return ret;
		}
	};


	template<class R, class D>
	struct HermiteKLocalBasisEvaluator<R, D, 1>
	{
		inline static R evaluate1D(const D& x, const int i, const int l)
		{
			switch (l)
			{
			case 0:
				return 1 - x;
			case 1:
				return -1;
			}
			return 0;
		}
	};


	template<class R, class D>
	struct HermiteKLocalBasisEvaluator<R, D, 2>
	{
		inline static R evaluate1D(const D& x, const int i, const int l)
		{
			const D x2 = x*x;

			if (l > 3) return 0;
			switch (i)
			{
			case 1:
				switch (l)
				{
				case 3:
					return 6.0;
					break;

				case 2:
					return x*6.0 - 4.0;
					break;

				case 1:
					return x2*3.0 - x*4.0 + 1.0;
					break;

				default: // 0
					return x2*-2.0 + x + x2*x;
					break;
				}
				break;

			case 0:
				switch (l)
				{
				case 3:
					return 1.2E1;
					break;

				case 2:
					return x*1.2E1 - 6.0;
					break;

				case 1:
					return x2*6.0 - x*6.0;
					break;

				default: // 0
					return x2*-3.0 + x2*x*2.0 + 1.0;
					break;
				}
				break;

			default:
				throw "Should be impossible.";
				break;
			}
			return 0;
		}
	};


	template<size_t dimDomain, size_t order, class X = double>
	class HermiteLocalBasis
	{
	private:
		using RT = FieldArray<X, 1>;
		using DT = FieldArray<X, dimDomain>;
		using DiffType = FieldArray<int, dimDomain>;

		const DT elementSize;
		FieldArray<size_t, dimDomain> axisPermutation;

	public:

		using RangeType = RT;
		using DifferentialType = DiffType;
		using DomainType = DT;

		enum {
			funcsPerNode = StaticPower<order, dimDomain>::power,
			funcsTotal = StaticPower<2, dimDomain>::power * funcsPerNode,
			polOrder = 2 * order - 1
		};

		HermiteLocalBasis(const X& elementSize) : HermiteLocalBasis(DT(elementSize)) {}

		HermiteLocalBasis(const DT& elementSize) : elementSize(elementSize)
		{
			// For now the axis permutation is always set to be trivial.
			// This might change in future.
			for (unsigned int i = 0; i < dimDomain; ++i)
				axisPermutation[i] = i;
		}

		inline X evaluate(DT x, const int function_index, DiffType alpha_ = DiffType(0)) const
		{
			X    ret = 1;

			// Apply permutation to differential operator.
			DiffType alpha;
			for (unsigned int j = 0; j < dimDomain; ++j)
				alpha[axisPermutation[j]] = alpha_[j];

			// Translate index i to its according tensor product, i.e.
			// determine the  necessary transformations on the x_j and
			// the 1D basis functions in use for the i-th basis function.
			//
			// We have
			//    i = cornerId * funcsPerNode + localId,
			//    cornerId \in \{0,\dots, 2^d-1\}
			//    localId \in \{0,\dots, k^d-1\}
			//    j-th bit of cornerId is j-th component of the associated coordinates
			//    localId = \sum_{j=0}^{d-1} hermiteId(j) * k^j.

			size_t  cornerId = function_index / funcsPerNode,
					localId = function_index % funcsPerNode;
			int     hermiteId = 0;
			X xj = 0;    // Will be modified depending on the corner.
			for (unsigned int j = 0; j < dimDomain; ++j)
			{
				// Extract relevant data.
				hermiteId = localId % order;
				xj = (cornerId & 1) ? (1 - x[j]) : x[j];
				ret *= (1 - 2 * (int)((alpha[j] ^ hermiteId) & cornerId & 1))
						* HermiteKLocalBasisEvaluator<X, X, order>::evaluate1D(xj, hermiteId, (int) alpha[j])
						* std::pow(elementSize[j], (X) (hermiteId-alpha[j])); // Note: That part *may* need to be adapted if the transformation Jacobians are no longer guaranteed to be diagonal matrices.

				// Prepare next iteration.
				localId /= order;
				cornerId >>= 1;
			}

			return ret;
		}


		inline auto evaluateAll(const DT& x, const DiffType& alpha = DiffType(0)) const
		{
			FieldArray<RT, funcsTotal> values;
			for (int i = 0; i < funcsTotal; ++i)
				values[i] = evaluate(x, i, alpha);
			return values;
		}

		inline auto gradients(const DT& x) const
		{
			FieldArray<DT, funcsTotal> grads;
			DiffType alpha = DiffType(0);
			for (int i = 0; i < funcsTotal; ++i)
			{
				for (unsigned int d = 0; d < dimDomain; ++d)
				{
					alpha[d] = 1;
					grads[i][d] = evaluate(x, i, alpha);
					alpha[d] = 0;
				}
			}
			return grads;
		}

		int size() const
		{
			return funcsTotal;
		}


		int polynomialOrder() const
		{
			return 2 * order - 1;
		}

		auto getGeometricIndex(int i) const
		{
			assert(0 <= i && i < funcsTotal);
			return std::make_tuple(0, i / funcsPerNode, i % funcsPerNode);
		}
	};

}

#endif // _FESUITE_LOCAL_FE_HERMITEK_