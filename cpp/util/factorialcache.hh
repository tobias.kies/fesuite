#ifndef _FESUITE_UTIL_FACTORIALCACHE_
#define _FESUITE_UTIL_FACTORIALCACHE_

#include <assert.h>
#include <vector>

namespace FeSuite
{
	class FactorialCache
	{
	private:
		using N = long long;
		static std::vector<N> factorial;
		
		FactorialCache() {}

	public:
		static inline auto evaluate(const N x)
		{
			assert(x >= 0); // There is a good reason not to make x an unsigned.
			N n = factorial.size();
			if (x+1 > n)
			{
				factorial.resize(x + 1);
				for (; n <= x; ++n)
					factorial[n] = n*factorial[n - 1];
			}
			return factorial[x];
		}

		static inline const auto& getFactorials()
		{
			return factorial;
		}
	};

	std::vector<typename FactorialCache::N> FactorialCache::factorial = { 1 };
}

#endif // _FESUITE_UTIL_FACTORIALCACHE_