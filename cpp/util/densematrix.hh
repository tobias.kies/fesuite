#ifndef _FESUITE_UTIL_DENSEMATRIX_
#define _FESUITE_UTIL_DENSEMATRIX_

#include <assert.h>

namespace FeSuite
{
	template<class T>
	class DenseMatrix
	{
	private:
		std::vector<std::vector<T>> data;
		int nRows;
		int nCols;

	public:
		DenseMatrix(int nRows = 0, int nCols = 0) : nRows(nRows), nCols(nCols)
		{
			data.resize(nRows);
			for (auto& row : data)
				row.resize(nCols);
		}

		auto rows() const
		{
			return nRows;
		}

		auto cols() const
		{
			return nCols;
		}

		std::vector<T>& operator[](int i)
		{
			assert(0 <= i && i < nRows);
			return data[i];
		}

		const std::vector<T>& operator[](int i) const
		{
			assert(0 <= i && i < nRows);
			return data[i];
		}
	};
}

#endif // _FESUITE_UTIL_FIELDVECTOR_