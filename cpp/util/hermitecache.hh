#ifndef _FESUITE_UTIL_HERMITECACHE_
#define _FESUITE_UTIL_HERMITECACHE_

#include "util/factorialcache.hh"

#include <assert.h>
#include <map>
#include <vector>

namespace FeSuite
{
	class HermiteCache
	{
	private:
		using N = int;
		using X = double;
		using Matrix = std::vector<std::vector<X>>; // Lazy matrix implementation. Could be improved.
		static std::map<N, Matrix> hermiteCoefficients;
		
		HermiteCache() {}

	public:
		static inline const auto& evaluate(const N k)
		{
			assert(k > 0);

			auto it = hermiteCoefficients.find(k);
			if (it != hermiteCoefficients.end())
				return it->second;

			auto& C = hermiteCoefficients[k];
			C.resize(k);
			for (int i = 0; i < k; ++i)
				C[i].resize(k);

			std::vector<X> hermiteVector(k + 1);
			for (int i = 0; i < k; ++i)    // i-th basis function
			{
				// The right hand side is always of type (0...010...0)
				// with i+k zeros in the beginning and
				// with (1..10..0) as nodes.
				// Consequently, we only have to start computing the
				// coefficients in the (i+k)-th row and (i+1)-th column.
				// Since this is a special case the Hermite scheme here
				// looks rather unusual.
				std::fill(hermiteVector.begin(), hermiteVector.end(), 0.0);
				hermiteVector[0] = 1. / FactorialCache::evaluate(i);
				for (int j = i; j < k; ++j)
				{
					for (int l = 1; l <= k; ++l)
						hermiteVector[l] -= hermiteVector[l - 1];
					hermiteVector[0] = 0;
					C[i][j] = hermiteVector[k];
				}
			}

			return C;
		}

		static inline const auto& getCoefficients()
		{
			return hermiteCoefficients;
		}
	};

	std::map<HermiteCache::N, HermiteCache::Matrix> HermiteCache::hermiteCoefficients;
}

#endif // _FESUITE_UTIL_HERMITECACHE_