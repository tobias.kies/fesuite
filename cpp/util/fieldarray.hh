#ifndef _FESUITE_UTIL_FIELDARRAY_
#define _FESUITE_UTIL_FIELDARRAY_

#include <array>
#include <assert.h>
#include <iostream>
#include <numeric>
#include <type_traits>

namespace FeSuite
{
	template<class T>
	class FieldVector;

	template<class T, int k>
	class FieldArray : public std::array<T, k>
	{
	private:
		using Base = std::array<T, k>;

	public:
		using Base::Base;

		FieldArray(const T& x = T())
		{
			for (int i = 0; i < k; ++i)
				Base::operator [](i) = x;
		}

		FieldArray(const Base& other)
		{
			for (int i = 0; i < k; ++i)
				Base::operator [](i) = other[i];
		}

		template<class S>
		FieldArray(const FieldArray<S, k>& other)
		{
			for (int i = 0; i < k; ++i)
				Base::operator [](i) = other[i];
		}

		template<class S>
		FieldArray(const FieldVector<S>& other)
		{
			assert(other.size() == k);
			for (int i = 0; i < k; ++i)
				Base::operator [](i) = other[i];
		}

		T operator*(const FieldArray<T, k>& other) const
		{
			T dot(0);
			for (int i = 0; i < k; ++i)
				dot += other[i] * Base::operator [](i);
			return dot;
			// return std::inner_product(other.begin(), other.end(), *this, 0); <- Does not work. Why?
		}

		FieldArray<T, k> operator*(const T& other) const
		{
			FieldArray<T, k> v;
			for (int i = 0; i < k; ++i)
				v[i] = other * Base::operator [](i);
			return v;
		}

		// template<class = typename std::enable_if<k==1>::type> // Does not work outside of Windows -> proper solution? Use "static_assert" for now.
		operator T() const
		{
			static_assert(k == 1, "Dimension of array must be 1.");
			return Base::operator [](0);
		}

		FieldArray<T, k> operator+=(const FieldArray<T, k>& other)
		{
			for (int i = 0; i < k; ++i)
				Base::operator [](i) += other[i];
			return *this;
		}

		friend std::ostream& operator <<(std::ostream& output, const FieldArray<T, k>& v)
		{
			for (int i = 0; i < k; ++i)
			{
				output << v[i];
				if (i + 1 != k)
					output << " ";
			}

			return output;
		}
	};
}

#endif // _FESUITE_UTIL_FIELDARRAY_