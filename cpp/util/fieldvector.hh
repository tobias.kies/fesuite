#ifndef _FESUITE_UTIL_FIELDVECTOR_
#define _FESUITE_UTIL_FIELDVECTOR_

#include <vector>

namespace FeSuite
{
	template<class T>
	class FieldVector : public std::vector<T>
	{
	private:
		using Base = std::vector<T>;

	public:
		using Base::Base;
	};
}

#endif // _FESUITE_UTIL_FIELDVECTOR_