#ifndef _FESUITE_UTIL_QUADRATURE_
#define _FESUITE_UTIL_QUADRATURE_

#include "util/quadrature_imp.hh"
#include "util/fieldvector.hh"
#include "grid/geometrytype.hh"

#include <cmath>
#include <map>

namespace FeSuite {
	namespace Quadrature {

		using X = double;

		struct QuadraturePoint
		{
			FieldVector<X> point;
			X weight;
		};

		using QuadratureRule = std::vector<QuadraturePoint>;

		std::map<int, std::map<int, QuadratureRule>> cache;

		template<size_t dim, class X = double>
		auto make_tensor_rule(const int order)
		{
			const auto rule_1D = Gauss1D<X>(order);
			const auto n = rule_1D.size();
			const int N = (int)std::pow((int)n, (int)dim);

			const auto&& multiIndex = [](int x, const int n) {
				std::array<int, dim> alpha;
				for (unsigned int i = 0; i < dim; ++i)
				{
					alpha[i] = x % n;
					x = (x - alpha[i]) / n;
				}
				return alpha;
			};

			QuadratureRule quadratureRule;
			quadratureRule.reserve(N);

			QuadraturePoint quadraturePoint;
			quadraturePoint.point.resize(dim);
			for (int i = 0; i < N; ++i)
			{
				const auto&& alpha = multiIndex(i, (int)n);

				quadraturePoint.weight = 1;
				for (unsigned int j = 0; j < dim; ++j)
				{
					quadraturePoint.point[j] = rule_1D.points[alpha[j]];
					quadraturePoint.weight *= rule_1D.weights[alpha[j]];
				}

				quadratureRule.push_back(quadraturePoint);
			}

			return quadratureRule;
		}

		template<class ElementGeometry, class X = double>
		const auto& rule(const ElementGeometry& e, const int order)
		{
			assert(ElementGeometry::type == GeometryType::TENSOR_CUBE); // The only supported topology at this moment.

			// Get dimension.
			const auto dim = ElementGeometry::dimension;

			// Compute rule if it does not exist yet.
			if (!cache.count(dim) || !cache[dim].count(order))
				cache[dim][order] = make_tensor_rule<dim, X>(order);

			// Return cached rule.
			return cache[dim][order];
		}
	}
}

#endif // _FESUITE_UTIL_QUADRATURE_
