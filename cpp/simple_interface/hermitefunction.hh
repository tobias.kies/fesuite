#ifndef _FESUITE_SIMPLEINTERFACE_HERMITEFUNCTION_
#define _FESUITE_SIMPLEINTERFACE_HERMITEFUNCTION_

// The following routines are kept relatively simple and specific in order to
// enable simple interfacing towards other programming languages or just
// faster usage through specialization in general.

#include "global_fe/globalhermitek.hh"
#include "global_fe/fe_function.hh"
#include "grid/tensorgrid.hh"

#include <array>
#include <stdexcept>
#include <vector>

namespace FeSuite
{
	template<size_t dim, size_t order>
	class HermiteFunction 
	{
	public:
		// Special type definitions that are assumed/required
		// to be compatible with the remaining base implementation.
		using X = double;

		using Grid = TensorGrid::TensorGrid<dim, X>;
		using AnsatzSpace = HermiteAnsatzSpace<Grid, dim, order, X>;
		using Function = FeFunction<AnsatzSpace,X>;

		using DiscretizationVectors = typename Grid::DiscretizationVectors;
		using DomainType = typename Function::DomainType;
		using DomainTypeVec = std::vector<DomainType>;
		using RangeType = X;
		using RangeTypeVec = std::vector<X>;
		using DifferentialType = typename Function::DifferentialType;
		using CoefficientType = typename Function::CoefficientType;

	protected:
		const Grid grid;
		const AnsatzSpace ansatzSpace;
		Function function;

	public:
		HermiteFunction(const DiscretizationVectors& discretizationVectors)
			: grid(discretizationVectors), ansatzSpace(grid), function(ansatzSpace)
		{}

		inline RangeType evaluate(const DomainType& x, const DifferentialType& alpha) const
		{
			return function.evaluate(x, alpha);
		}

		inline RangeTypeVec evaluate_vec(const DomainTypeVec& xVec, const DifferentialType& alpha) const
		{
			/*// Somewhat inefficient: Copy operation. -> Got solved by introducing an additional template parameter to FeFunction
			const auto vec_ = function.evaluate(xVec, alpha);
			RangeTypeVec vec(vec_.size());
			for (int i = 0; i < vec.size(); ++i)
				vec[i] = vec_[i];
			return vec;*/
			return function.evaluate(xVec, alpha);
		}

		CoefficientType& get_coefficients()
		{
			return function.getCoefficients();
		}

		const Grid& get_grid() const
		{
			return grid;
		}

		const AnsatzSpace& get_ansatz_space() const
		{
			return ansatzSpace;
		}
	};
}

#endif