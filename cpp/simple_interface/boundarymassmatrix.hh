#ifndef _FESUITE_SIMPLEINTERFACE_BOUNDARYMASSMATRIX_
#define _FESUITE_SIMPLEINTERFACE_BOUNDARYMASSMATRIX_

// The following routines are kept relatively simple and specific in order to
// enable simple interfacing towards other programming languages or just
// faster usage through specialization in general.

#include "assembler/operatorassembler.hh"
#include "assembler/localassembler/boundarymassassembler.hh"
#include "simple_interface/hermitefunction.hh"

namespace FeSuite
{
	template<size_t dim, size_t order>
	DenseMatrix<double> assemble_boundary_mass_matrix(const HermiteFunction<dim, order>& f)
	{
		using F = HermiteFunction<dim, order>;
		using AnsatzSpace = typename F::AnsatzSpace;
		using LocalAssembler = LocalBoundaryMassAssembler<AnsatzSpace>;
		using Assembler = OperatorAssembler<AnsatzSpace, LocalAssembler>;

		const AnsatzSpace& ansatzSpace = f.get_ansatz_space();

		LocalAssembler localAssembler;
		Assembler assembler(ansatzSpace, localAssembler);
		const auto&& M = assembler.assemble();

		return M;
	}
}

#endif