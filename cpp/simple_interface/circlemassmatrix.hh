#ifndef _FESUITE_SIMPLEINTERFACE_CIRCLEMASSMATRIX_
#define _FESUITE_SIMPLEINTERFACE_CIRCLEMASSMATRIX_

#include "assembler/operatorassembler.hh"
#include "assembler/localassembler/weightedmassassembler.hh"
#include "simple_interface/hermitefunction.hh"

namespace FeSuite
{
	template<size_t dim, size_t order>
	DenseMatrix<double> assemble_circle_mass_matrix(
		const HermiteFunction<dim, order>& f,
		const typename HermiteFunction<dim, order>::DomainType& center,
		const double radius)
	{
		const auto weighting_function = [&center, &radius](const auto& x) -> double
		{
			double distance = 0;
			for (unsigned int i = 0; i < dim; ++i) {
				distance += std::pow(x[i] - center[i], 2);
			}
			return (distance <= radius * radius);
		};

		using F = HermiteFunction<dim, order>;
		using AnsatzSpace = typename F::AnsatzSpace;
		using LocalAssembler = LocalWeightedMassAssembler<AnsatzSpace, decltype(weighting_function)>;
		using Assembler = OperatorAssembler<AnsatzSpace, LocalAssembler>;

		const AnsatzSpace& ansatzSpace = f.get_ansatz_space();

		LocalAssembler localAssembler(weighting_function);
		Assembler assembler(ansatzSpace, localAssembler);
		const auto&& M = assembler.assemble();

		return M;
	}
}

#endif