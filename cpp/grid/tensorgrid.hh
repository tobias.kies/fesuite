#ifndef _FESUITE_GRID_TENSORGRID_
#define _FESUITE_GRID_TENSORGRID_

#include "grid/geometrytype.hh"
#include "util/fieldarray.hh"

#include <algorithm>
#include <array>
#include <assert.h>
#include <memory>
#include <numeric>
#include <vector>

namespace FeSuite
{
	namespace TensorGrid
	{

		template<size_t dim>
		class ElementImp : public std::array<int, dim>
		{
		private:
			using Base = std::array<int, dim>;

		public:
			using Base::Base;
			ElementImp()
			{
				for (unsigned int i = 0; i < dim; ++i)
					Base::operator [](i) = 0;
			}
			static const int dimension = dim;
		};

		template<class ElementGeometry, class X = double>
		class ElementFaceGeometry // This class is a quick workaround in order to
			// have access to faces of elements. A more elegant appraoch would be
			// to recursively introduce iterators for elements over elements 
			// with increased co-dimension. But that will require some more
			// careful handling.
		{
		protected:
			static const size_t dim = ElementGeometry::dimension;
			const ElementGeometry& elementGeometry;
			const int faceId;

		public:
			static const GeometryType type = ElementGeometry::type;
			static const int dimension = ElementGeometry::dimension - 1;

			ElementFaceGeometry(const ElementGeometry& elementGeometry, const int faceId)
			: elementGeometry(elementGeometry), faceId(faceId)
			{
				assert(0 <= faceId && faceId <= 3);
			}

			bool isBoundary() const
			{
				if (dim != 2) {
					throw "ElementFaceGeometry::isBoundary currently only supports dim == 2";
				}
				const auto& element = elementGeometry.getElement();
				const auto& discretizationVectors = elementGeometry.getDiscretizationVectors();
				if (element[1] == 0 && faceId == 0) {
					return true;
				}
				if (element[0] == 0 && faceId == 1) {
					return true;
				}
				if (element[1] + 2 == discretizationVectors[1].size() && faceId == 2) {
					return true;
				}
				if (element[0] + 2 == discretizationVectors[0].size() && faceId == 3) {
					return true;
				}
				return false;
			}

			auto volume() const
			{
				if (dim != 2) {
					throw "ElementFaceGeometry::volume currently only supports dim == 2";
				}
				const auto lengths = elementGeometry.edgeLengths();
				unsigned int affectedDimension = faceId % 2;
				return lengths[affectedDimension];
			}

			auto applyEmbedding(const FieldArray<X, dim-1>& x_lowerDimensional) const
			{
				if (dim != 2) {
					throw "ElementFaceGeometry::applyEmbedding currently only supports dim == 2";
				}
				FieldArray<X, dim> x;
				unsigned int affectedDimension = faceId % 2;
				x[affectedDimension] = x_lowerDimensional[0];
				x[1 - affectedDimension] = (faceId >= 2);
				return x;
			}
		};

		template<size_t dim, class DiscretizationVectors, class X = double>
		class ElementGeometry
		{
		protected:
			using Element = ElementImp<dim>;
			const Element& element;
			const DiscretizationVectors& v;

		public:
			ElementGeometry(const Element& element, const DiscretizationVectors& v)
				: element(element), v(v) {}

			static const GeometryType type = GeometryType::TENSOR_CUBE;
			static const int dimension = Element::dimension;

			auto edgeLengths() const
			{
				FieldArray<X, dim> lengths;
				for (unsigned int i = 0; i < dim; ++i)
					lengths[i] = v[i][element[i]+1] - v[i][element[i]];
				return lengths;
			}

			auto volume() const
			{
				const auto lengths = edgeLengths();
				return std::accumulate(lengths.begin(), lengths.end(), 1.0, std::multiplies<X>());
			}

			auto local(const FieldArray<X, dim>& x_global) const
			{
				FieldArray<X, dim> x_local;
				for (unsigned int i = 0; i < dim; ++i)
					x_local[i] = (x_global[i] - v[i][element[i]]) / (v[i][element[i] + 1] - v[i][element[i]]);
				return x_local;
			}

			auto global(const FieldArray<X, dim>& x_local) const
			{
				FieldArray<X, dim> x_global;
				for (unsigned int i = 0; i < dim; ++i) {
					x_global[i] = v[i][element[i]] + x_local[i] * (v[i][element[i] + 1] - v[i][element[i]]);
				}
				return x_global;
			}

			auto getNumFaces() const
			{
				return 2*dim;
			}

			auto getFaceGeometry(const int faceId) const
			{
				auto face = ElementFaceGeometry(*this, faceId);
				return face;
			}

			const auto getElement() const
			{
				return element;
			}

			const auto getDiscretizationVectors() const
			{
				return v;
			}
		};

		template<size_t dim>
		class ElementIterator // Could save a few lines in this class if the whole class were within the scope of TensorGrid.
										// Code is taken and adapted from a generic iterator example.
		{
		private:
			using N_Dim = std::array<int, dim>;
			const N_Dim& numElementsPerAxis;
			const int numElements;
			using T = ElementImp<dim>;

		public:
			ElementIterator(const std::array<int, dim>& numElementsPerAxis)
				: numElementsPerAxis(numElementsPerAxis), numElements(std::accumulate(numElementsPerAxis.begin(), numElementsPerAxis.end(), 1, std::multiplies<int>())) {}

			static inline T iToElement(int id, const N_Dim& numElementsPerAxis)
			{
				T element = T();
				for (unsigned int k = 0; k < dim; ++k)
				{
					element[k] = id % numElementsPerAxis[k];
					id = (id - element[k]) / numElementsPerAxis[k];
				}
				return element;
			}

			class iterator
			{
			public:
				typedef iterator self_type;
				typedef T value_type;
				typedef T reference;
				typedef int pointer;
				typedef std::forward_iterator_tag iterator_category;
				typedef int difference_type;
				iterator(pointer ptr, const N_Dim& numElementsPerAxis) : ptr_(ptr), numElementsPerAxis(numElementsPerAxis) { }
				self_type operator++() { self_type i = *this; ptr_++; return i; }
				self_type operator++(int junk) { ptr_++; return *this; }
				reference operator*() { return iToElement(ptr_, numElementsPerAxis); }
				pointer operator->() { return std::unique_ptr<T>(iToElement(ptr_, numElementsPerAxis)); }
				bool operator==(const self_type& rhs) { return ptr_ == rhs.ptr_; }
				bool operator!=(const self_type& rhs) { return ptr_ != rhs.ptr_; }
			private:
				pointer ptr_;
				const N_Dim& numElementsPerAxis;
			};

			class const_iterator
			{
			public:
				typedef const_iterator self_type;
				typedef T value_type;
				typedef T& reference;
				typedef int pointer;
				typedef int difference_type;
				typedef std::forward_iterator_tag iterator_category;
				const_iterator(pointer ptr, const N_Dim& numElementsPerAxis) : ptr_(ptr), numElementsPerAxis(numElementsPerAxis) { }
				self_type operator++() { self_type i = *this; ptr_++; return i; }
				self_type operator++(int junk) { ptr_++; return *this; }
				reference operator*() { return iToElement(ptr_); }
				const pointer operator->() { return std::unique_ptr<T>(iToElement(ptr_, numElementsPerAxis)); }
				bool operator==(const self_type& rhs) { return ptr_ == rhs.ptr_; }
				bool operator!=(const self_type& rhs) { return ptr_ != rhs.ptr_; }
			private:
				pointer ptr_;
				const N_Dim& numElementsPerAxis;
			};

			iterator begin()
			{
				return iterator(0, numElementsPerAxis);
			}

			iterator end()
			{
				return iterator(numElements, numElementsPerAxis);
			}

			const_iterator begin() const
			{
				return const_iterator(0, numElementsPerAxis);
			}

			const_iterator end() const
			{
				return const_iterator(numElements, numElementsPerAxis);
			}
		};


		template<size_t dim, class X = double>
		class TensorGrid
		{
		protected:
			using DiscretizationVector = std::vector<X>;
			using DiscretizationVectors_ = std::array<DiscretizationVector, dim>;

			const DiscretizationVectors_ discretization_vectors;
			int numNodes_;
			int numElements_;
			std::array<int, dim> nodeStrides;
			std::array<int, dim> numElementsPerAxis;

			void checkWellDefinedness() const
			{
				for (unsigned int i = 0; i < dim; ++i)
				{
					const auto& v = discretization_vectors[i];
					if(v.size() <= 1)
						throw "Discretization vectors must have length > 1.";
					if (!std::is_sorted(v.begin(), v.end()))
						throw "Discretization vectors must be sorted in ascending order.";
				}
			}

			void init()
			{
				numNodes_ = 1;
				numElements_ = 1;
				for (unsigned int i = 0; i < dim; ++i)
				{
					numNodes_ *= (int) discretization_vectors[i].size();
					numElements_ *= (int) discretization_vectors[i].size()-1;
					nodeStrides[i] = (i == 0) ? 1 : (int) discretization_vectors[i].size() * nodeStrides[i - 1];
					numElementsPerAxis[i] = (int) discretization_vectors[i].size() - 1;
				}
			}

		public:
			using Element = ElementImp<dim>;
			using DiscretizationVectors = DiscretizationVectors_;

			TensorGrid(const DiscretizationVectors& discretization_vectors) : discretization_vectors(discretization_vectors)
			{
				checkWellDefinedness();
				init();
			}

			int numNodes() const
			{
				return numNodes_;
			}

			int numElements() const
			{
				return numElements;
			}

			int index(Element element, const int subEntityDim, int subEntityId) const // Note intentional pass by values here!
			{
				if (subEntityDim != 0)
					throw "NotImplemented: TensorGrid::index with subEntityDim != 0";
				// Apply some magic here which exploits the specific data structure of the type Element.
				for (unsigned int i = 0; i < dim; ++i, subEntityId >>= 1)
					element[i] += (subEntityId & 0x01);
				const auto idx = std::inner_product(element.begin(), element.end(), nodeStrides.begin(), 0);
				return idx;
			}

			auto elements() const
			{
				return ElementIterator<dim>(numElementsPerAxis);
			}

			auto geometry(const Element& element) const
			{
				return ElementGeometry<dim, DiscretizationVectors, X>(element, discretization_vectors);
			}

			template<class GlobalCoordinate>
			inline auto containingElement(const GlobalCoordinate& x) const
			{
				assert(x.size() == dim);
				Element element;

				for (unsigned int i = 0; i < dim; ++i)
				{
					const auto& v = discretization_vectors[i];
					const int n = (int)v.size();
					assert(v[0] <= x[i] && x[i] <= v[n - 1]);

					auto it = std::upper_bound(v.begin(), v.end(), x[i]);
					element[i] = std::min((int) (it - v.begin() - 1), n-2);
				}

				return element;
			}
		};
	}
}

#endif // _FESUITE_GRID_TENSORGRID_