#ifndef _FESUITE_ASSEMBLER_OPERATORASSEMBLER_
#define _FESUITE_ASSEMBLER_OPERATORASSEMBLER_

#include "util/densematrix.hh"

namespace FeSuite
{
	template<class AnsatzSpace, class LocalAssembler>
	class OperatorAssembler
	{
	protected:
		using Matrix = DenseMatrix<double>; // To-do: Switch to a sparse matrix format in future.

		const AnsatzSpace& ansatzSpace;
		const LocalAssembler& localAssembler;

	public:
		OperatorAssembler(const AnsatzSpace& ansatzSpace, const LocalAssembler& localAssembler = LocalAssembler())
			: ansatzSpace(ansatzSpace), localAssembler(localAssembler) {}

		Matrix assemble() const
		{
			Matrix M(ansatzSpace.size(), ansatzSpace.size());

			const auto& grid = ansatzSpace.getGrid();
			for (const auto& element : grid.elements())
			{
				const auto globalIndices = ansatzSpace.getGlobalIndices(element);
				const auto&& localMatrix = localAssembler.assemble(ansatzSpace, element);
				for (int i = 0; i < localMatrix.rows(); ++i)
					for (int j = 0; j < localMatrix.cols(); ++j)
						M[globalIndices[i]][globalIndices[j]] += localMatrix[i][j];
			}

			return M;
		}
	};
}

#endif // _FESUITE_ASSEMBLER_OPERATORASSEMBLER_