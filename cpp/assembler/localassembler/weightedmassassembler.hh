#ifndef _FESUITE_ASSEMBLER_LOCALASSEMBLER_WEIGHTEDMASSASSEMBLER_
#define _FESUITE_ASSEMBLER_LOCALASSEMBLER_WEIGHTEDMASSASSEMBLER_

#include "util/quadrature.hh"

namespace FeSuite
{
	template<class AnsatzSpace, class WeightingFunction>
	class LocalWeightedMassAssembler
	{
	protected:
		using Element = typename AnsatzSpace::Grid::Element;
		using X = double;
		using Matrix = DenseMatrix<X>;

	public:
		LocalWeightedMassAssembler(const WeightingFunction& weightingFunction)
			: weightingFunction(weightingFunction)
		{}

		Matrix assemble(const AnsatzSpace& ansatzSpace, const Element& element) const
		{
			const auto&& localBasis = ansatzSpace.getLocalBasis(element);
			const int n = localBasis.size();
			const auto&& geometry = ansatzSpace.getGrid().geometry(element);
			Matrix M(n, n);

			// Get quadrature rule.
			const auto& quadRule = Quadrature::rule(geometry, localBasis.polynomialOrder() * 2);
			const auto volume = geometry.volume();

			// Execute quadrature.
			for (const auto& point_weight : quadRule)
			{
				const auto& point = point_weight.point;
				const auto globalPoint = geometry.global(point);
				const auto& weight = point_weight.weight * weightingFunction(globalPoint);
				const auto integrationElement = weight * volume;

				// Evaluate basis functions.
				const auto&& value = localBasis.evaluateAll(point);

				// Update local matrix.
				for (int i = 0; i < n; ++i)
					for (int j = 0; j < n; ++j)
						M[i][j] += (value[i] * value[j]) * integrationElement;
			}

			return M;
		}

	private:
		const WeightingFunction& weightingFunction;
	};
}

#endif //_FESUITE_ASSEMBLER_LOCALASSEMBLER_WEIGHTEDMASSASSEMBLER_