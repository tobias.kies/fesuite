#ifndef _FESUITE_ASSEMBLER_LOCALASSEMBLER_LAPLACEASSEMBLER_
#define _FESUITE_ASSEMBLER_LOCALASSEMBLER_LAPLACEASSEMBLER_

#include "util/quadrature.hh"

namespace FeSuite
{
	template<class AnsatzSpace>
	class LocalLaplaceAssembler
	{
	protected:
		using Element = typename AnsatzSpace::Grid::Element;
		using X = double;
		using Matrix = DenseMatrix<X>;

	public:
		LocalLaplaceAssembler() {}

		Matrix assemble(const AnsatzSpace& ansatzSpace, const Element& element) const
		{
			const auto&& localBasis = ansatzSpace.getLocalBasis(element);
			const int n = localBasis.size();
			const auto&& geometry = ansatzSpace.getGrid().geometry(element);
			Matrix M(n, n);

			// Get quadrature rule.
			const auto& quadRule = Quadrature::rule(geometry, localBasis.polynomialOrder() * 2);
			const auto volume = geometry.volume();

			// Execute quadrature.
			for (const auto& point_weight : quadRule)
			{
				const auto& point = point_weight.point;
				const auto& weight = point_weight.weight;
				const auto integrationElement = weight * volume;

				// Evaluate gradients.
				const auto&& gradient = localBasis.gradients(point);
				/*std::cerr << element[0] << element[1] << " " << point[0] << " " << point[1] << " " << weight << " " << volume << std::endl;
				for (int i = 0; i < gradient.size(); ++i)
					std::cerr << "\t" << gradient[i][0] << " " << gradient[i][1] << std::endl;*/
				//std::cerr << element[0] << " " << point[0] << " " << weight << " " << volume << std::endl;
				//for (int i = 0; i < gradient.size(); ++i)
				//	std::cerr << "\t" << gradient[i][0] << std::endl;

				// Update local matrix.
				for (int i = 0; i < n; ++i)
					for (int j = 0; j < n; ++j)
						M[i][j] += (gradient[i] * gradient[j]) * integrationElement;
			}

			//for(int i = 0; i < n; ++i)
			//{
			//	for (int j = 0; j < n; ++j)
			//		std::cerr << M[i][j] << " ";
			//	std::cerr << std::endl;
			//}
			//std::cerr << std::endl;

			return M;
		}
	};
}

#endif //_FESUITE_ASSEMBLER_LOCALASSEMBLER_LAPLACEASSEMBLER_