#ifndef _FESUITE_ASSEMBLER_LOCALASSEMBLER_BOUNDARYMASSASSEMBLER_
#define _FESUITE_ASSEMBLER_LOCALASSEMBLER_BOUNDARYMASSASSEMBLER_

#include "util/quadrature.hh"

namespace FeSuite
{
	// Remark: This whole assembler would be more efficient if we would iterate only over
	//		   boundary elements right away.
	template<class AnsatzSpace>
	class LocalBoundaryMassAssembler
	{
	protected:
		using Element = typename AnsatzSpace::Grid::Element;
		using X = double;
		using Matrix = DenseMatrix<X>;

	public:
		LocalBoundaryMassAssembler() {}

		Matrix assemble(const AnsatzSpace& ansatzSpace, const Element& element) const
		{
			const auto&& localBasis = ansatzSpace.getLocalBasis(element);
			const int n = localBasis.size();
			const auto&& geometry = ansatzSpace.getGrid().geometry(element);
			Matrix M(n, n);

			for (unsigned int faceId = 0; faceId < geometry.getNumFaces(); ++faceId) {
				const auto faceGeometry = geometry.getFaceGeometry(faceId);
				if (!faceGeometry.isBoundary())
					continue;
				const auto& quadRule = Quadrature::rule(faceGeometry, localBasis.polynomialOrder() * 2);
				const auto volume = faceGeometry.volume();
				for (const auto& point_weight : quadRule) {
					const auto point = faceGeometry.applyEmbedding(point_weight.point);
					const auto& weight = point_weight.weight;
					const auto integrationElement = weight * volume;

					// Evaluate basis functions.
					const auto&& value = localBasis.evaluateAll(point);

					// Update local matrix.
					for (int i = 0; i < n; ++i)
						for (int j = 0; j < n; ++j)
							M[i][j] += (value[i] * value[j]) * integrationElement;
				}
			}

			return M;
		}
	};
}

#endif //_FESUITE_ASSEMBLER_LOCALASSEMBLER_BOUNDARYMASSASSEMBLER_