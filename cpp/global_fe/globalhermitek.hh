#ifndef _FESUITE_GLOBALFE_GLOBALHERMITEK_
#define _FESUITE_GLOBALFE_GLOBALHERMITEK_

#include "local_fe/hermitek.hh"

#include <array>

namespace FeSuite {
	template<class Grid_, size_t dimDomain, size_t order, class X = double>
	class HermiteAnsatzSpace
	{
	protected:
		const Grid_& grid;
		using LocalSpace = HermiteLocalBasis<dimDomain, order, X>;
		using Element = typename Grid_::Element;

	public:
		using DomainType = typename LocalSpace::DomainType;
		using DifferentialType = typename LocalSpace::DifferentialType;
		using RangeType = typename LocalSpace::RangeType;
		using FieldType = X;
		using Grid = Grid_;

		HermiteAnsatzSpace(const Grid& grid) : grid(grid) {}

		int size() const
		{
			return grid.numNodes() * LocalSpace::funcsPerNode;
		}

		const Grid& getGrid() const
		{
			return grid;
		}

		auto getGlobalIndices(const Element& element) const
		{
			std::array<int, LocalSpace::funcsTotal> indices;
			const auto edgeLengths = grid.geometry(element).edgeLengths();
			const LocalSpace localSpace(edgeLengths);
			int subEntityDim, subEntityId, multiplicity;
			for (int i = 0; i < LocalSpace::funcsTotal; ++i)
			{
				std::tie(subEntityDim, subEntityId, multiplicity) = localSpace.getGeometricIndex(i);
				indices[i] = grid.index(element, subEntityDim, subEntityId) * LocalSpace::funcsPerNode + multiplicity;
				//std::cerr << element[0] << element[1] << " " << i << "." << subEntityDim << "." << subEntityId << "." << multiplicity << " " << grid.index(element, subEntityDim, subEntityId) << " " << indices[i] << std::endl;
			}
			return indices;
		}

		auto getLocalBasis(const Element& element) const
		{
			return LocalSpace(grid.geometry(element).edgeLengths());
		}
	};
}

#endif // _FESUITE_GLOBALFE_GLOBALHERMITEK_