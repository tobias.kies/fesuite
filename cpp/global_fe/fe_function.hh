#ifndef _FESUITE_GLOBAL_FE_FE_FUNCTION_
#define _FESUITE_GLOBAL_FE_FE_FUNCTION_

#include "util/fieldvector.hh"

#include <algorithm>
#include <assert.h>
#include <vector>

namespace FeSuite
{

	template<class AnsatzSpace, class RangeType_ = typename AnsatzSpace::RangeType, class CoefficientType_ = FieldVector<typename AnsatzSpace::FieldType>>
	class FeFunction
	{
	protected:
		const AnsatzSpace& ansatzSpace;
		CoefficientType_ coefficients;

	public:
		using DomainType = typename AnsatzSpace::DomainType;
		using DifferentialType = typename AnsatzSpace::DifferentialType;
		using RangeType = RangeType_;
		using FieldType = typename AnsatzSpace::FieldType;
		using CoefficientType = CoefficientType_;

		FeFunction(const AnsatzSpace& ansatzSpace) : ansatzSpace(ansatzSpace), coefficients(ansatzSpace.size()) {}

		inline RangeType evaluate(const DomainType& x, const DifferentialType& alpha = DifferentialType(0)) const
		{
			const auto&& element = ansatzSpace.getGrid().containingElement(x); // Warning: This can be potentially expensive!
			const auto&& localBasis = ansatzSpace.getLocalBasis(element);

			const auto&& global_indices = ansatzSpace.getGlobalIndices(element);
			const auto&& x_local = ansatzSpace.getGrid().geometry(element).local(x);
			const auto&& local_values = localBasis.evaluateAll(x_local, alpha);
			assert(global_indices.size() == local_values.size());

			RangeType value(0);
			for (int i = 0; i < localBasis.size(); ++i)
				value += coefficients[global_indices[i]] * local_values[i];

			return value;
		}

		inline auto evaluate(const std::vector<DomainType>& x, const DifferentialType& alpha = DifferentialType(0)) const
		{
			std::vector<RangeType> values(x.size());
			for (unsigned int i = 0; i < x.size(); ++i)
				values[i] = evaluate(x[i], alpha);
			return values;
		}

		auto& getCoefficients()
		{
			return coefficients;
		}

		const auto& getCoefficients() const
		{
			return coefficients;
		}
	};

}

#endif