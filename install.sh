#!/bin/bash

set -e

# Make sure that all necessary dependencies are installed.
apt-get install -y python3
pip install -r requirements.txt

# Carry out the actual installation.
pip uninstall -y fesuite
cd python/fe_suite
python generate_template_files.py
pip install --global-option=build_ext -e .
cd ../..

# Make plausibility tests.
if [ $(pip list | grep "fesuite " | wc -l) == "0" ]; then
    echo "Plausibility check failed: Package does not seem to be installed."
    exit 1
fi
echo "Checking if package import works..."
python -c "import fesuite; _ = fesuite.make_hermite_function"
echo "Installation finished successfully."
