import itertools
from mpl_toolkits.mplot3d import Axes3D  # noqa: F401
import matplotlib.pyplot as plt
import numpy as np
from fesuite import make_hermite_function, assemble_laplace_matrix
import random


# Define problem parameters.
dim = 2
order = 2
grid = [np.arange(11) for i in range(dim)]

max_deriv = 1.5 if dim == 1 else 1
max_curv = 1.0
max_corner_value = 1
fix_corner_value = True
nSamples = 100 if dim == 1 else 10
random.seed()


# Create a random sample. Note: When creating many samples there would be some
# work in here that should be moved outside of this function.
def create_sample():
    # Create function space and regularization matrix.
    # ATTENTION: Currently, matrix assembly is still dense.
    #            In future, there should be a transition to sparse matrices.
    f = make_hermite_function(dim, order, grid)
    A = assemble_laplace_matrix(f)

    # Retrieve size of discretization vector for f.
    N = len(f)

    # Collect all first derivative and laplace-relevant DOFs.
    # Note: This is relatively hard-coded. There maybe should rather be a
    #       utility function (or something similar) to get that job done.
    dofs_per_node = order**dim
    I_deriv_1 = [np.arange(1+d, N+1, dofs_per_node) for d in range(dim)] \
        if order > 1 else []
    I_deriv_2 = [np.arange(1+dim+(d-1)*dim+d, N+1, dofs_per_node) for d in range(dim)] \
        if order > 2 else []

    # Join the derivative nodes as this example does not need any further
    # differentiation between the directions here.
    I_deriv = list(itertools.chain.from_iterable(I_deriv_1 + I_deriv_2))
    I_deriv_1 = list(itertools.chain.from_iterable(I_deriv_1))
    I_deriv_2 = list(itertools.chain.from_iterable(I_deriv_2))
    I_deriv.sort()  # Not necessary but makes interactive output prettier.

    # In this example the set of prescribed values shall coincide with the set
    # of derivatives plus the very first and very last function value indices.
    I_pre = [0] + I_deriv + [N-dofs_per_node]

    # Define the prescribed values for f.
    # Here: Choose random values between 0 and max_deriv for the derivaatives
    #       and fixed bounds for the rest.
    corner_value = max_corner_value if fix_corner_value \
        else random.random() * max_corner_value
    J_deriv_1 = [i for i in range(len(I_pre)) if I_pre[i] in I_deriv_1]
    J_deriv_2 = [i for i in range(len(I_pre)) if I_pre[i] in I_deriv_2]
    f_pre = np.zeros(len(I_pre))
    f_pre[J_deriv_1] = np.array([random.random() for i in range(len(J_deriv_1))]) \
        * max_deriv * corner_value / max_corner_value
    f_pre[J_deriv_2] = np.array([random.random() for i in range(len(J_deriv_2))]) \
        * max_curv * corner_value / max_corner_value
    f_pre[0] = 0
    f_pre[-1] = corner_value

    # Extract a submatrix for smoothing and solve the induced system.
    I_post = list(set(range(N)) - set(I_pre))
    A_sub = A[np.ix_(I_post, I_post)]
    b = -A[np.ix_(I_post, I_pre)].dot(f_pre)
    f_post = np.linalg.solve(A_sub, b)

    # Now only prescribe the function values and let all the derivatives (also
    # higher order ones) do the relaxing. This reduces the amount of
    # fluctuations between the nodes. (An issue which only becomes visible
    # when plotting the resulting functions with respect to a finer grid.)
    I_func = np.arange(0, N, dofs_per_node)
    # somewhat clumsy way to extract all possible prescribed function
    # values into the constraints
    f_func = np.zeros(N)
    f_func[I_pre] = f_pre
    f_func[I_post] = f_post
    f_func = f_func[I_func]
    I_not_func = list(set(range(N)) - set(I_func))

    A_sub = A[np.ix_(I_not_func, I_not_func)]
    b = -A[np.ix_(I_not_func, I_func)].dot(f_func)
    f_not_func = np.linalg.solve(A_sub, b)

    # Combine results.
    u = f.coefficients
    # Note: Most of the values here will be overwritten differently
    #       in just two lines. Lazy coding.
    u[I_pre] = f_pre
    u[I_post] = f_post
    u[I_not_func] = f_not_func

    return f


fig = plt.figure()
if dim == 2:
    ax = fig.add_subplot(111, projection="3d")
for n in range(nSamples):
    f = create_sample()
    if dim == 1:
        x = np.linspace(min(grid[0]), max(grid[0]), 100)
        y = f.evaluate_vec(x)
        plt.plot(x, y)
    if dim == 2:
        G = [np.linspace(min(g), max(g), 10) for g in grid]
        X, Y = np.meshgrid(*G)
        x = X.flatten()
        y = Y.flatten()
        eval_points = np.stack((x, y), axis=-1)
        z = f.evaluate_vec(eval_points)
        if nSamples < 15:
            ax.plot_trisurf(x, y, z)
        else:
            ax.scatter(x, y, z)

plt.show()
