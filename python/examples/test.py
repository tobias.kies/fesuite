import fesuite

from fesuite import make_hermite_function, assemble_laplace_matrix
import numpy as np

print(dir(fesuite))

a = np.array([[1, 2, 3]])
# f = PyHermiteFunction_1_2(a)
f = make_hermite_function(1, 3, a)
f[0] = 2

c = f.get_coefficients()
print(c)
c[0] = 1.0
print(f.get_coefficients())

print('f direct print', f)

x = np.array([1])
y = f.evaluate(x)
print('y', y)


x_vec = [x, x, x, x]
y_vec = f.evaluate_vec(x_vec)
print('y_vec', y_vec)

A = assemble_laplace_matrix(f)
print(A)
