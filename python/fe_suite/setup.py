from setuptools import setup, Extension
from Cython.Build import cythonize
import numpy


PYX_PATH = 'pyhermitefunction.pyx'
CPP_PATH = '../../cpp'

EXTENSION_NAME = 'fesuite'
DESCRIPTION = '''
A simple interface to create basic finite element discretizations using higher
order finite elements.
'''
VERSION = '0.0.1'

AUTHOR = 'Tobias Kies'
AUTHOR_EMAIL = 'tobias.kies@gmx.de'
URL = 'https://gitlab.com/tobias.kies/fesuite'

EXTENSION = Extension(
    name=EXTENSION_NAME,
    sources=[PYX_PATH],
    include_dirs=[CPP_PATH, numpy.get_include()],
    extra_compile_args=["-std=c++17", "-O3"],
    language="c++"
)

# Compile the code into pyd.
setup(
    name=EXTENSION_NAME,
    description=DESCRIPTION,
    version=VERSION,
    author=AUTHOR,
    author_email=AUTHOR_EMAIL,
    url=URL,
    ext_modules=cythonize(EXTENSION)
)
