cdef extern from "simple_interface/hermitefunction.hh" namespace "FeSuite":
    cdef cppclass DomainType_%{0}_%{1} "FeSuite::HermiteFunction<%{0}, %{1}>::DomainType":
        DomainType_%{0}_%{1}() except+
        double& operator[](size_t)

    cdef cppclass DiffType_%{0}_%{1} "FeSuite::HermiteFunction<%{0}, %{1}>::DifferentialType":
        DiffType_%{0}_%{1}() except+
        int& operator[](size_t)

    cdef cppclass DiscretizationVectors_%{0}_%{1} "FeSuite::HermiteFunction<%{0}, %{1}>::DiscretizationVectors":
        DiscretizationVectors_%{0}_%{1}() except+
        vector[double]& operator[](size_t)

    cdef cppclass HermiteFunction_%{0}_%{1} "FeSuite::HermiteFunction<%{0}, %{1}>":
        HermiteFunction_%{0}_%{1}(const DiscretizationVectors_%{0}_%{1}& discretizationVectors)
        double evaluate(const DomainType_%{0}_%{1}& x, const DiffType_%{0}_%{1}& alpha) except +
        vector[double] evaluate_vec(const vector[DomainType_%{0}_%{1}]& xVec, const DiffType_%{0}_%{1}& alpha) except +
        vector[double]& get_coefficients()


cdef extern from "simple_interface/laplacematrix.hh" namespace "FeSuite":
    cdef DenseMatrix assemble_laplace_matrix_%{0}_%{1} "FeSuite::assemble_laplace_matrix<%{0}, %{1}>"(const HermiteFunction_%{0}_%{1}& f)


cdef extern from "simple_interface/massmatrix.hh" namespace "FeSuite":
    cdef DenseMatrix assemble_mass_matrix_%{0}_%{1} "FeSuite::assemble_mass_matrix<%{0}, %{1}>"(const HermiteFunction_%{0}_%{1}& f)


cdef extern from "simple_interface/circlemassmatrix.hh" namespace "FeSuite":
    cdef DenseMatrix assemble_circle_mass_matrix_%{0}_%{1} "FeSuite::assemble_circle_mass_matrix<%{0}, %{1}>"(
        const HermiteFunction_%{0}_%{1}& f,
        const DomainType_%{0}_%{1}& center,
        const double radius
    )


cdef extern from "simple_interface/boundarymassmatrix.hh" namespace "FeSuite":
    cdef DenseMatrix assemble_boundary_mass_matrix_%{0}_%{1} "FeSuite::assemble_boundary_mass_matrix<%{0}, %{1}>"(const HermiteFunction_%{0}_%{1}& f)

