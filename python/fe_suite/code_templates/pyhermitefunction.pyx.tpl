cdef class PyHermiteFunction_%{0}_%{1}:

    cdef HermiteFunction_%{0}_%{1}* _c_hermite_function
    cdef size_t _dim
    cdef size_t _order
    cpdef _grid

    def __cinit__(self, discretizationVectors):
        self._dim = len(discretizationVectors)
        self._order = %{1}
        self._grid = discretizationVectors
        assert(self._dim == %{0})

        cdef DiscretizationVectors_%{0}_%{1} c_discretizationVectors;
        for i in range(self._dim):
            for j in range(len(discretizationVectors[i])):
                c_discretizationVectors[i].push_back(discretizationVectors[i][j])

        self._c_hermite_function = new HermiteFunction_%{0}_%{1}(c_discretizationVectors)


    def __dealloc__(self):
        del self._c_hermite_function


    def evaluate(self, x, alpha = None):
        cdef DomainType_%{0}_%{1} c_x
        if(self._dim == 1 and not isinstance(x, Iterable)):
            c_x[0] = x
        else:
            for i in range(self._dim):
                c_x[i] = x[i]

        cdef DiffType_%{0}_%{1} c_alpha
        if(self._dim == 1 and not isinstance(alpha, Iterable)):
            c_alpha[0] = alpha if alpha is not None else 0.0
        else:
            for i in range(self._dim):
                c_alpha[i] = alpha[i] if alpha is not None else 0.0

        return self._c_hermite_function.evaluate(c_x, c_alpha)


    def evaluate_vec(self, xVec, alpha = None):
        cdef vector[DomainType_%{0}_%{1}] c_xVec
        c_xVec.resize(len(xVec))

        if(self._dim == 1 and len(xVec) > 0 and not isinstance(xVec[0], Iterable)):
            for k in range(len(xVec)):
                c_xVec[k][0] = xVec[k]
        else:
            for k in range(len(xVec)):
                for i in range(self._dim):
                    c_xVec[k][i] = xVec[k][i]

        cdef DiffType_%{0}_%{1} c_alpha
        if(self._dim == 1 and not isinstance(alpha, Iterable)):
            c_alpha[0] = alpha if alpha is not None else 0.0
        else:
            for i in range(self._dim):
                c_alpha[i] = alpha[i] if alpha is not None else 0.0

        # For now, just make a lot of copies.
        # To-do: Refine this in future.
        #        Also maybe enable the use of more flexible input shapes.
        cdef vector[double] data = self._c_hermite_function.evaluate_vec(c_xVec, c_alpha)

        cdef np.npy_intp size
        size = data.size()
        z = np.PyArray_SimpleNew(1, &size, np.NPY_DOUBLE)
        for i in range(data.size()):
            z[i] = data[i]

        return z


    @property
    def coefficients(self):
        cdef double* data = &(self._c_hermite_function.get_coefficients()[0])
        cdef np.npy_intp size = self._c_hermite_function.get_coefficients().size()
        cdef np.ndarray coeff = np.PyArray_SimpleNewFromData(1, &size, np.NPY_DOUBLE, data);
        return coeff

    @property
    def dim(self):
        return self._dim

    @property
    def grid(self):
        return self._grid

    @property
    def order(self):
        return self._order

    @property
    def dofs_per_node(self):
        return self._order**self._dim

    @property
    def num_nodes(self):
        return int(len(self) / self.dofs_per_node) # Yes, there probably is a more elegant way to do this.

    # Convenience-features that allows to treat the the HermiteFunction both
    # as a vector and a function.
    def __getitem__(self, i):
        return self._c_hermite_function.get_coefficients()[i]

    def __setitem__(self, i, val):
        self._c_hermite_function.get_coefficients()[i] = val

    def __len__(self):
        return self._c_hermite_function.get_coefficients().size()

    def __call__(self, x):
        return self.evaluate(x)

    def __str__(self):
        return py_np.array(self._c_hermite_function.get_coefficients()).__str__()


# Note the potentially inefficient copy here.
cpdef py_assemble_laplace_matrix_%{0}_%{1}(PyHermiteFunction_%{0}_%{1} f):
    cdef DenseMatrix M = assemble_laplace_matrix_%{0}_%{1}(cython.operator.dereference(f._c_hermite_function))

    cdef np.npy_intp size[2]
    size[0] = M.rows()
    size[1] = M.cols()
    A = np.PyArray_SimpleNew(2, size, np.NPY_DOUBLE)

    for i in range(M.rows()):
        for j in range(M.cols()):
            A[i,j] = M[i][j]

    return A


# Note the potentially inefficient copy here.
cpdef py_assemble_mass_matrix_%{0}_%{1}(PyHermiteFunction_%{0}_%{1} f):
    cdef DenseMatrix M = assemble_mass_matrix_%{0}_%{1}(cython.operator.dereference(f._c_hermite_function))

    cdef np.npy_intp size[2]
    size[0] = M.rows()
    size[1] = M.cols()
    A = np.PyArray_SimpleNew(2, size, np.NPY_DOUBLE)

    for i in range(M.rows()):
        for j in range(M.cols()):
            A[i,j] = M[i][j]
    
    return A


# Note the potentially inefficient copy here.
cpdef py_assemble_circle_mass_matrix_%{0}_%{1}(
        PyHermiteFunction_%{0}_%{1} f,
        center,
        radius):
    cdef DomainType_%{0}_%{1} c_center
    if(%{0} == 1 and not isinstance(center, Iterable)):
        c_center[0] = center
    else:
        for i in range(%{0}):
            c_center[i] = center[i]

    cdef DenseMatrix M = assemble_circle_mass_matrix_%{0}_%{1}(
        cython.operator.dereference(f._c_hermite_function),
        c_center,
        radius
    )

    cdef np.npy_intp size[2]
    size[0] = M.rows()
    size[1] = M.cols()
    A = np.PyArray_SimpleNew(2, size, np.NPY_DOUBLE)

    for i in range(M.rows()):
        for j in range(M.cols()):
            A[i,j] = M[i][j]

    return A


# Note the potentially inefficient copy here.
cpdef py_assemble_boundary_mass_matrix_%{0}_%{1}(PyHermiteFunction_%{0}_%{1} f):
    cdef DenseMatrix M = assemble_boundary_mass_matrix_%{0}_%{1}(cython.operator.dereference(f._c_hermite_function))

    cdef np.npy_intp size[2]
    size[0] = M.rows()
    size[1] = M.cols()
    A = np.PyArray_SimpleNew(2, size, np.NPY_DOUBLE)

    for i in range(M.rows()):
        for j in range(M.cols()):
            A[i,j] = M[i][j]

    return A
