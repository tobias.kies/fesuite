from collections.abc import Iterable
import copy
from datetime import datetime
import glob
import itertools
import os
from pathlib import Path


# Create range over covered orders and dimensions.
vec_dimensions = [1, 2, 3, 4]
vec_orders = [1, 2, 3, 4]

# For debugging purposes it is recommended to make the choice below:
# vec_dimensions = [1]
# vec_orders = [2]

# Set up the list of variable configurations for substitution.
base_variables = [vec_dimensions, vec_orders, [str(vec_dimensions)], [str(vec_orders)]]
variables = list(itertools.product(*base_variables))

string_disclaimer = ("# This is an auto-generated file from a template engine.\n"
                     "# Any changes made to this file may be overwritten automatically\n"
                     "# This build is from {0}\n\n\n")
path_templates = "code_templates/"
path_output = ""


def buildSubTemplateFile(path, variables_):
    template = ""
    code = ""

    variables = copy.deepcopy(variables_)

    if(len(variables) == 0):
        return code

    nVariables = 1
    if isinstance(variables[0], Iterable):
        nVariables = len(variables[0])

    with open(path, "r") as input_file:
        template = input_file.read()

    present_variables = [i for i in range(nVariables) if ("%%{%d}" % i) in template]

    for i in range(len(present_variables)):
        template = template.replace("%%{%d}" % present_variables[i], "{%d}" % i)

    for k in range(len(variables)):
        variables[k] = tuple([variables[k][i] for i in present_variables])

    variables = list(set(variables))

    for variable_combination in variables:
        code += template.format(*variable_combination)

    return code


def buildTemplateFile(filename, variables):
    basename = filename[:-4]
    stem = Path(filename).stem
    code = string_disclaimer.format(datetime.now().strftime('%Y-%m-%d %H:%M'))

    pathHeader = basename + ".tplhdr"
    pathBody = filename
    pathFooter = basename + ".tplftr"

    if(os.path.exists(pathHeader)):
        code += buildSubTemplateFile(pathHeader, variables) + '\n'
    if(os.path.exists(pathBody)):
        code += buildSubTemplateFile(pathBody, variables) + '\n'
    if(os.path.exists(pathFooter)):
        code += buildSubTemplateFile(pathFooter, variables)

    with open(path_output + stem, "w") as output_file:
        output_file.write(code)


template_files = [f for f in glob.glob(path_templates + "*.tpl")]
for filename in template_files:
    print('Processing file %s' % filename)
    buildTemplateFile(filename, variables)
